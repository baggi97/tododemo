todosApp.filter('statusFiler', function() {
    return function(input, status) {
        if(!status || status === '')
            return input;

        var statusBoolean = false;
        if(status === 'done')
            statusBoolean = true;

        var ouput = input.filter(function(todo) {
            return todo.done === statusBoolean;
        });
        return ouput;
    };
});
