todosApp.directive('todoItem',function() {
    'use strict';



    function link(scope, element, attrs) {
        var inputElement = angular.element( element.find('input') );

        var ESCAPE_KEY = 27;
        var oldValue = '';

        scope.isEditing = false;

        scope.editTodo = function() {
            oldValue = scope.editableText;
            scope.isEditing = true;

            element.addClass( 'active' );
            inputElement[0].focus();
        };

        scope.saveTodo = function() {
            scope.isEditing = false;
        };

        element.bind('keydown', function (event) {
			if (event.keyCode === ESCAPE_KEY) {
				scope.editableText = oldValue;
                inputElement[0].blur();
			}
		});

		scope.$on('$destroy', function () {
			element.unbind('keydown');
		});
    }

    return {
        scope : {
            editableText: '=editableText',
        },
        templateUrl: 'todoItem.html',
        link: link
    };
});
