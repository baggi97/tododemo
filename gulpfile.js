var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var config = require('./config.json');

var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var autoprefix = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var glob = require('gulp-sass-glob');
var sourcemaps = require('gulp-sourcemaps');
var scssLint = require('gulp-scss-lint');
var jshint = require('gulp-jshint');
var minifyjs = require('gulp-js-minify');
var concat = require('gulp-concat');

gulp.task('css', function() {
  return gulp.src(config.css.src)
    .pipe(glob())
    .pipe(plumber({
      errorHandler: function (error) {
        notify.onError({
          title:    "Gulp",
          subtitle: "Failure!",
          message:  "Error: <%= error.message %>",
          sound:    "Beep"
        }) (error);
        this.emit('end');
      }}))
    .pipe(sourcemaps.init())
    .pipe(sass({
      style: 'compressed',
      errLogToConsole: true,
      includePaths: config.css.includePaths
    }))
    .pipe(autoprefix('last 2 versions', '> 1%', 'ie 9', 'ie 10'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.css.dest))
    .pipe(browserSync.reload({ stream: true, match: '**/*.css' }));
});

// JS Linting.
gulp.task('js-lint', function() {
  return gulp.src(config.js.src)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('js', function(){
  gulp.src(config.js.src)
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(minifyjs())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.js.dest));
});

gulp.task('watch', function() {
    gulp.watch(config.css.src, ['css']);
    gulp.watch(config.js.src, ['js-lint']);
    gulp.watch(config.js.src, ['js']);
});

gulp.task('default', ['js','css', 'watch']);
