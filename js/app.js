var todosApp = angular.module('todos', ['ngRoute', 'ngResource']);

todosApp.config(function ($routeProvider) {
	'use strict';

	var routeConfig = {
		controller: 'todoController',
		templateUrl: 'main.html',
        resolve : {

        }
	};

	$routeProvider
		.when('/', routeConfig)
		.otherwise({
			redirectTo: '/'
		});
});
