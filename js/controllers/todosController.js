todosApp.controller('todoController', function TodoCtrl($scope, $routeParams, $filter, localStorage) {
		'use strict';

        var todoList = $scope.todoList = localStorage.get();

        $scope.newTodoTitle = '';
        $scope.status = '';

        $scope.$watch('todoList', function () {
			$scope.notDoneCount = $filter('filter')(todoList, { done: false }).length;
			$scope.doneCount = todoList.length - $scope.notDoneCount;
            localStorage.put(todoList);
		}, true);

        $scope.createTodo = function() {
            if(!$scope.newTodoTitle)
                return;

            var newTodo = {
                title : $scope.newTodoTitle,
                done : false
            };

            todoList.push(newTodo);
            $scope.newTodoTitle = '';
        };

        $scope.deleteTodo = function(todo) {
            todoList.splice(todoList.indexOf(todo), 1);
        };

        $scope.filterList = function(status) {
            $scope.status = status;
        };

        $scope.clearDone = function() {
            var incompleteTodos = todoList.filter(function (todo) {
				return !todo.done;
			});

			angular.copy(incompleteTodos, todoList);
        };
	});
